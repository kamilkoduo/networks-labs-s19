#ifndef _STACK_
#define _STACK_

#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct StackNode{
    int data;
    struct StackNode* next;
}  Node;


int peek(Node** top);

void push(Node** top, int data);

int pop(Node** top);

int isEmpty(Node* root);

char* display(Node *top);

Node* newNode(int data);

int size(Node* top);

#endif
