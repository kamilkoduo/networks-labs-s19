#include "stack.h"
#include <sys/types.h>
#include <unistd.h>
#include <signal.h>

void server(int* fd) {
  Node *stacktop = NULL;
  int parameters[2];
  char* answer = (char*)malloc(sizeof(char)*500);
  while(1){
    raise(SIGSTOP);
    read(fd[0],parameters, sizeof(parameters));
    switch (parameters[0]) {
      case 1:
          snprintf(answer,500,"Peek result: %d\n", peek(&stacktop));
          break;
      case 2:
          push(&stacktop, parameters[1]);
          break;
      case 3:
          snprintf(answer,500,"Pop result: %d\n", pop(&stacktop));
          break;
      case 4:
          snprintf(answer,500,isEmpty(stacktop)?"EMPTY\n":"NON-EMPTY\n");
          break;
      case 5:
          snprintf(answer,500,"stack:\n%s\n", display(stacktop));
          break;
      case 6:
          stacktop = NULL;
          snprintf(answer,500,"New empty stack is created\n");
          break;
      case 7:
          snprintf(answer,500,"Stack size: %d\n", size(stacktop));
          break;
    }
    printf("%s\n", answer);
  }
}
void client(pid_t pid, int* fd){
  printf("Hello, I am a little client process here! \nEmpty stack is created\n");
  int parameters[2];
  while (1) {
    printf("\n%s\n", "Follow the instructions:\n0 - QUIT\n1 - PEEK\n2 - PUSH\n3 - POP\n4 - EMPTY\n5 - DISPLAY\n6 - CREATE\n7 - SIZE\n");

    scanf("%d", &parameters[0]);
    if (parameters[0]==2){
      scanf("%d", &parameters[1]);
    }
    else if (parameters[0]==0){
        kill(pid, SIGKILL);
        raise(SIGKILL);
    }
    write(fd[1],parameters, sizeof(parameters));
    kill(pid, SIGCONT);
  }
}

int main(int argc, char** args){

  int fd[2];
  pipe(fd);
  pid_t pid = fork();
  if(pid ==-1){
    printf("ERROR\n");
    //error
  }else if (pid){
    //client
    client(pid, fd);
  }else{
    //server here
    server(fd);
  }
}
