#include "stack.h"

void concatenate(char* t, char* s);

Node* newNode(int data)
{
    Node* node = (Node*) malloc(sizeof(Node));
    node->data = data;
    node->next = NULL;
    return node;
}

int peek(Node** top)
{
    if (isEmpty(*top))
        return INT_MIN;
    return (*top)->data;
}

int isEmpty(Node* root)
{
    return !root;
}

void push(Node** top, int data)
{
    Node* node = newNode(data);
    node->next = (struct StackNode *)*top;
    *top = node;

    //printf("%d pushed to stack\n", data);
}

int pop(Node** top)
{
    if (isEmpty(*top))
        return INT_MIN;

    Node* temp = *top;
    *top = (Node*)(*top)->next;
    int popped = temp->data;
    free(temp);

    return popped;
}

int size(Node *top){
  int s = 0;
  Node* tmp = top;
  while(tmp){
    tmp = (Node*)tmp->next;
    s++;
  }
  return s;
}

char* display(Node *top){
  char* result_string = (char*) malloc(20*sizeof(char)*size(top));

  Node *temp_node = top;
  while(!isEmpty(temp_node)){
    char* tmp = (char*)malloc(20*sizeof(char));
    snprintf(tmp,10,"%d\n", temp_node->data);
    concatenate(result_string, tmp);
    temp_node = (Node*)temp_node->next;
  }
  return result_string;
}

void concatenate(char* t, char* s) {
   int c=0, d=0;
   while (t[c] != '\0') {
      c++;
   }
   while (s[d] != '\0') {
      t[c] = s[d];
      d++;
      c++;
   }
   t[c] = '\0';
}
